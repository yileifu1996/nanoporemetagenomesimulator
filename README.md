# NanoporeMetagenomeSimulator

Simulation of metagenome community, based on Nanosim-h, supports even abundance and lognormal abundance.

### Requirement:

    Nanosim-h: https://github.com/karel-brinda/NanoSim-H
    