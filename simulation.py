import os
from multiprocessing import Pool
import re
import argparse
from Bio import SeqIO
import random
import numpy as np
from tqdm import tqdm

NANOSIMH_DEFAULT_COVERAGE_RATIO = 0.006
NANOSIMH_FILESIZE_READNUM_RATION = 7800


def parse_args():
    """
    Function of parsing Args
    """
    parser = argparse.ArgumentParser(
        description="Metagenome Community Simulator. Author: yilei.fu@rice.edu", formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    required_args = parser.add_argument_group("Required args")
    required_args.add_argument(
        "-i", "--ref", help="Path to reference folder", type=str, required=True)
    parser.add_argument('--version', action='version', version='0.1')
    parser.add_argument(
        "-e", "--even", help="Even abundance, default is lognormal abundance", action="store_true")
    parser.add_argument(
        "-n", "--otu_num", help="Maximum number of genomes can be drawn from an OTU", type=int, default=3)
    parser.add_argument(
        "-c", "--coverage", help="Coverage of abundance, uniform coverage for even abundance and mean coverage for lognormal abundance", type=int, default=10)
    parser.add_argument(
        "-t", "--threads", help="Threads number", type=int, default=32, action="store")
    parser.add_argument("-o", "--out", help="Output directory",
                        type=str, default="work/")
    args = parser.parse_args()

    return args


def truncated_geometric_distribution(mu, k):
    return pow((1 - 1 / mu), k) * (1 / mu)


def random_x_from_tgd(m):
    prob_list = []
    for k in range(m):
        prob_list.append(truncated_geometric_distribution(m / 2, k))
    p = np.array(prob_list)
    p /= p.sum()
    x = np.random.choice(range(1, m + 1), p=p)
    return x


def read_fasta(input_folder, ref_path, out_num):
    otu_seqs = {}
    fasta_file_list = os.listdir(input_folder)
    for fasta_file in tqdm(fasta_file_list):
        temp_record = []
        # temp_seqs = []
        handle = list(SeqIO.parse(os.path.join(
            input_folder, fasta_file), "fasta"))
        if len(handle) < out_num:
            temp_record = handle

        else:
            choose_num = random_x_from_tgd(out_num)
            temp_record = random.choices(handle, k=choose_num)
        otu_seqs.update({fasta_file: temp_record})
        #TODO: More Comprehensive filename parsing
        fasta_name = fasta_file.split(".fa")[0]

        selected_fasta_name = f"{fasta_name}_selected.fasta"
        selected_fasta_path = os.path.join(ref_path, selected_fasta_name)
        with open(selected_fasta_path, "w") as f:
            for record in temp_record:
                SeqIO.write(record, f, "fasta")
    # os.system(f"cat {ref_path}* > {work_folder}combined_ref.fasta")
    # print(f"cat {ref_path}* > {work_folder}combined_ref.fasta")
    return otu_seqs


def calc_read_number(coverage, input_file):
    input_file_size = os.stat(input_file).st_size
    read_size = coverage * input_file_size
    read_num = int(read_size / NANOSIMH_FILESIZE_READNUM_RATION)
    return read_num


def run_nanosimh(read_number, out_file, input_file):
    os.system(f"nanosim-h -n {read_number} -o {out_file} {input_file}")


def lognormal_abundance(coverage, ref_path, threads, out_dir):
    """
    Make the coverage of all OTU's genomes lognormal distribution
    """
    ref_file_list = os.listdir(ref_path)
    ref_num = len(ref_file_list)
    lognormal_draw = np.random.lognormal(1, 2, ref_num)
    lognormal_draw /= lognormal_draw.mean()
    lognormal_draw *= coverage
    ref_cov_dict = {}
    for i, ref_file in enumerate(ref_file_list):
        ref_cov_dict.update({ref_file:lognormal_draw[i]})
    if ref_num < threads:
        process_pool = Pool(ref_num)
    else:
        process_pool = Pool(threads)

    for i in range(ref_num):
        file_n = ref_file_list[i].split(".fa")[0]
        input_file = os.path.join(ref_path, ref_file_list[i])
        read_num = calc_read_number(ref_cov_dict[ref_file_list[i]], input_file)
        out_file = os.path.join(out_dir, f"{file_n}_simulated_reads.fasta")
        process_pool.apply_async(run_nanosimh, args=(read_num, out_file, input_file))

    print('Waiting for all subprocesses done...')
    process_pool.close()
    print("log!")


def even_abundance(coverage, ref_path, threads, out_dir):
    """
    Make the coverage of all OTU's genomes same
    """
    ref_file_list = os.listdir(ref_path)
    ref_num = len(ref_file_list)
    if ref_num < threads:
        process_pool = Pool(ref_num)
    else:
        process_pool = Pool(threads)

    for i in range(ref_num):
        file_n = ref_file_list[i].split(".fa")[0]
        input_file = os.path.join(ref_path, ref_file_list[i])
        read_num = calc_read_number(coverage, input_file)
        out_file = os.path.join(out_dir, f"{file_n}_simulated_reads.fasta")
        process_pool.apply_async(run_nanosimh, args=(read_num, out_file, input_file))

    print('Waiting for all subprocesses done...')
    process_pool.close()
    # process_pool.join()



if __name__ == "__main__":
    args = parse_args()
    otu_num = args.otu_num
    work_dir = args.out
    os.mkdir(work_dir)
    coverage = args.coverage
    threads = args.threads
    input_dir = args.ref

    # ref_path is the path stores selected references.
    ref_path = os.path.join(work_dir, "selected_refs/")
    os.mkdir(ref_path)

    reads_path = os.path.join(work_dir, "simulated_reads/")
    os.mkdir(reads_path)
    print("Generating Selected Reference File from OTUs...")
    read_fasta(input_dir, ref_path, otu_num)
    if args.even:
        even_abundance(coverage, ref_path, threads, reads_path)
    else:
        lognormal_abundance(coverage, ref_path, threads, reads_path)
    logs_dir = os.path.join(work_dir, "nanosim-h_logs")
    errors_dir = os.path.join(work_dir, "nanosim-h_errors")
    os.mkdir(logs_dir)
    os.mkdir(errors_dir)
    os.system(f"mv {reads_path}*.log {logs_dir}")
    os.system(f"mv {reads_path}*.errors.txt {errors_dir}")

    print(args)
